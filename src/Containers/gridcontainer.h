#ifndef GRIDCONTAINER_H
#define GRIDCONTAINER_H

#include "container.h"

DECLARE_SMART(GridContainer, spGridContainer)

class GridContainer: public Container
{
    struct actorData{
        oxygine::Vector2 pos;
        oxygine::Vector2 size;
    };

public:
    GridContainer();
    void add(oxygine::spActor item, oxygine::Vector2 pos, oxygine::Vector2 size={1,1});
    virtual void resizeItems() override;

private:
    int cols = 0;
    int rows = 0;
    std::vector<actorData> itemsData;
};

#endif // GRIDCONTAINER_H
