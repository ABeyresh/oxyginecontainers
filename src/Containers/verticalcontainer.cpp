#include "verticalcontainer.h"

using namespace oxygine;

VerticalContainer::VerticalContainer()
{
}

void VerticalContainer::resizeItems()
{
    oxygine::Vector2 newSize, newPosition;
    newSize = {
        getWidth()-(margins.right+margins.left),
        (getHeight()-(margins.bottom+margins.top)-spacing*(objects.size()-1))/float(objects.size())
    };
    newPosition.x = margins.left;
    for(uint i = 0; i < objects.size(); i++){
        newPosition.y = margins.top + (newSize.y+spacing)*i;
        objects[i]->setSize(newSize);
        objects[i]->setPosition(newPosition);
    }
}
