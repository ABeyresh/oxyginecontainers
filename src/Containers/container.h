#ifndef CONTAINER_H
#define CONTAINER_H


#include <oxygine-framework.h>
#include <oxygine/actor/Actor.h>
#include <vector>

DECLARE_SMART(Container, spContainer);

enum class StretchType{
    ToLeft,
    Stretch
};

class Container: public oxygine::Actor
{
public:
    Container();
    virtual ~Container();
    void setMargin(int margin);
    void setMargin(int vertical, int horizontal);
    void setMargin(int top, int bottom, int left, int right);
    void setSpacing(int spacing);
    void add(oxygine::spActor actor);
    virtual void resizeItems() = 0;
    void removeLast();
    oxygine::spColorRectSprite selfColorRect;
    oxygine::spColorRectSprite marginRect;

    void setStretch(StretchType type);

protected:
    virtual void sizeChanged(const oxygine::Vector2& size) override;

protected:
    struct{
        int top = 10,
            bottom = 10,
            left = 10,
            right = 10;
    } margins;
    int spacing = 10;
    StretchType stretchType = StretchType::Stretch;
    std::vector<oxygine::spActor> objects;
};

#endif // CONTAINER_H
