#ifndef VERTICALCONTAINER_H
#define VERTICALCONTAINER_H


#include "container.h"

using std::vector;

DECLARE_SMART(VerticalContainer, spVecticalContainer);

class VerticalContainer: public Container
{
public:
    VerticalContainer();
    virtual void resizeItems() override;

};

#endif // VERTICALCONTAINER_H
