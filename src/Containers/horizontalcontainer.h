#ifndef HORIZONTALCONTAINER_H
#define HORIZONTALCONTAINER_H

#include "container.h"

DECLARE_SMART(HorizontalContainer, spHorizontalContainer)

class HorizontalContainer: public Container
{
public:
    HorizontalContainer();
    virtual void resizeItems() override;

};

#endif // HORIZONTALCONTAINER_H
