#include "horizontalcontainer.h"

HorizontalContainer::HorizontalContainer()
{

}

void HorizontalContainer::resizeItems()
{
    oxygine::Vector2 newSize, newPosition;
    newSize = {
        (getWidth()-(margins.right+margins.left)-spacing*(objects.size()-1))/float(objects.size()),
        getHeight()-(margins.bottom+margins.top)
    };
    newPosition.y = margins.top;
    for(uint i = 0; i < objects.size(); i++){
        newPosition.x = margins.left+ (newSize.x+spacing)*i;
        objects[i]->setSize(newSize);
        objects[i]->setPosition(newPosition);
    }
}
