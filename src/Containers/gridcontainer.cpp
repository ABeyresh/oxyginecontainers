#include "gridcontainer.h"

GridContainer::GridContainer()
{

}

#include <iostream>
using std::cout;
using std::endl;

void GridContainer::add(oxygine::spActor item,
                        oxygine::Vector2 pos,
                        oxygine::Vector2 size)
{
    itemsData.push_back({pos, size});
    if(pos.x > cols)
        cols = pos.x;
    if(pos.y > rows)
        rows = pos.y;
    cout<<"Item added " << pos.x << ", "<< pos.y <<endl;
    cout<<"Items range " << cols << ", "<< rows <<endl;
    Container::add(item);
}

void GridContainer::resizeItems()
{
    oxygine::Vector2 freeField{
        getWidth()-margins.left-margins.right-spacing*(cols-1),
        getHeight()-margins.top-margins.bottom-spacing*(rows-1),
    };
    oxygine::Vector2 cellSize{
        freeField.x/cols,
        freeField.y/rows
    };

    oxygine::Vector2 newSize, newPosition;
    int currentX, currentY;
    for(uint i = 0; i < objects.size(); i++){
        newSize = {
            cellSize.x*itemsData[i].size.x,
            cellSize.y*itemsData[i].size.y
        };
        currentX = margins.left+(itemsData[i].pos.x-1)*cellSize.x+(itemsData[i].pos.x-1)*spacing;
        currentY = margins.top+(itemsData[i].pos.y-1)*cellSize.y+(itemsData[i].pos.y-1)*spacing;
        newPosition.x = currentX;
        newPosition.y = currentY;
        objects[i]->setSize(newSize);
        objects[i]->setPosition(newPosition);

    }
}
