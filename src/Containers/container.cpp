#include "container.h"

using namespace oxygine;

Container::Container()
{

    marginRect = new ColorRectSprite();
    selfColorRect = new ColorRectSprite();
    marginRect->setColor(Color(120, 120, 120, 255));
    selfColorRect->setColor(Color(200, 120, 120, 255));
    marginRect->setPosition(margins.left, margins.top);
    selfColorRect->setSize(getSize());
    marginRect->setSize({getSize().x-margins.left-margins.right, getSize().y-margins.bottom-margins.top});
    selfColorRect->attachTo(this);
    marginRect->attachTo(this);
}

Container::~Container()
{
    selfColorRect->detach();
    selfColorRect = 0;
    marginRect->detach();
    marginRect = 0;
    for(auto& i: objects){
        i->detach();
        i = 0;
    }
}

void Container::setMargin(int margin)
{
    margins.top = margin;
    margins.bottom = margin;
    margins.left = margin;
    margins.right = margin;
    marginRect->setPosition(margins.left, margins.top);
}

void Container::setMargin(int vertical, int horizontal)
{
    margins.top = vertical;
    margins.bottom = vertical;
    margins.left = horizontal;
    margins.right = horizontal;
    marginRect->setPosition(margins.left, margins.top);
}

void Container::setMargin(int top, int bottom, int left, int right)
{
    margins.top = top;
    margins.bottom = bottom;
    margins.left = left;
    margins.right = right;
    marginRect->setPosition(margins.left, margins.top);
}

void Container::setSpacing(int spacing)
{
    this->spacing = spacing;
    resizeItems();
}

void Container::add(oxygine::spActor actor)
{
    objects.push_back(actor);
    addChild(actor);
    resizeItems();
}

void Container::removeLast()
{
    if(objects.empty())
        return;
    auto& last = objects.back();
    last->detach();
    last = 0;
    objects.pop_back();
    resizeItems();
}

void Container::sizeChanged(const oxygine::Vector2 &size)
{
    marginRect->setPosition(margins.left, margins.top);
    selfColorRect->setSize(size);
    marginRect->setSize({size.x-margins.left-margins.right, size.y-margins.bottom-margins.top});
    resizeItems();
}
