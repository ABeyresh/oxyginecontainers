#include "example.h"
#include "verticalcontainer.h"
#include "horizontalcontainer.h"
#include <ctime>

using namespace oxygine;

Game* Game::selfInstance = nullptr;

Game & Game::instance()
{
    if (!selfInstance)
        selfInstance = new Game();
    return *selfInstance;
}

Game::Game()
{
    hContainer = new HorizontalContainer;
    vContainer = new VerticalContainer;
    hContainer->selfColorRect->setColor(Color(0x19381f));
    hContainer->marginRect->setColor(Color(0xeee82c));
    vContainer->selfColorRect->setColor(Color(0x19381f));
    vContainer->marginRect->setColor(Color(0xeee82c));
    complexContainer = new HorizontalContainer;
    gContainer = new GridContainer;
}

void Game::init()
{
    srand(time(0));
    spColorRectSprite addButton = new ColorRectSprite;
    addButton->setColor(Color(255, 255, 255, 255));
    addButton->setSize(50, 20);
    addButton->setPosition(500, 50);
    addButton->addClickListener([this](Event* e){
        spColorRectSprite rect1 = new ColorRectSprite;
        rect1->setColor(Color(1+rand()%255, 1+rand()%255, 1+rand()%255, 255));
        vContainer->add(rect1);
        spColorRectSprite rect2 = new ColorRectSprite;
        rect2->setColor(Color(1+rand()%255, 1+rand()%255, 1+rand()%255, 255));
        hContainer->add(rect2);
    });

    spColorRectSprite spacingButton = new ColorRectSprite;
    spacingButton->setColor(Color(180, 180, 180, 255));
    spacingButton->setSize(50, 20);
    spacingButton->setPosition(570, 50);
    spacingButton->addClickListener([this](Event* e){
        vContainer->removeLast();
        hContainer->removeLast();
    });
    spColorRectSprite r1 = new ColorRectSprite;
    r1->setColor(Color(200, 150, 100));
    spColorRectSprite r2 = new ColorRectSprite;
    r2->setColor(Color(200, 150, 100));

    spColorRectSprite r3 = new ColorRectSprite;
    r2->setColor(Color(200, 150, 100));
    gContainer->add(r1, {1, 2});
    gContainer->add(r2, {3, 1});
    gContainer->add(r3, {1, 1});


    auto size = getStage()->getSize();
    complexContainer->setSpacing(20);
    complexContainer->setPosition(0, 0);
    complexContainer->setSize(size.x/2, size.y/2);
    gContainer->setPosition(size.x/2, size.y/2);
    gContainer->setSize(size.x/2, size.y/2);
    complexContainer->add(vContainer);
    complexContainer->add(hContainer);
    getStage()->addChild(gContainer);
    getStage()->addChild(complexContainer);
    getStage()->addChild(addButton);
    getStage()->addChild(spacingButton);
}

void Game::destroy()
{
    hContainer->detach();
    vContainer->detach();
    getStage()->removeChildren();
}
