#pragma once
#include "oxygine-framework.h"
#include "container.h"
#include "gridcontainer.h"

using namespace oxygine;

class Game{
public:
    static Game& instance();

    void init();
    void destroy();

private:
    static Game* selfInstance;

    Game();
    spContainer hContainer;
    spContainer vContainer;
    spContainer complexContainer;
    spGridContainer gContainer;
};
